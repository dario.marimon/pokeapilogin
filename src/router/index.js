import { createRouter, createWebHistory } from "vue-router";
import authRouter from "@/modules/auth/router";
import pokemonRouter from "@/modules/pokemon/router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      redirect: { name: "login" },
    },
    {
      path: "/auth",
      ...authRouter,
    },
    {
      path: "/pokemon",
      ...pokemonRouter,
    },
  ],
});

export default router;
