const routes = {
  name: "pokemon",
  redirect: { name: "list" },
  component: () => import("../views/PokemonLayout.vue"),
  children: [
    {
      path: "list",
      name: "list",
      component: () => import("../views/ListView.vue"),
    },
    // {
    //   path: ":name",
    //   name: "name",
    //   component: () => import("../views/RegisterView.vue"),
    // },
  ],
};

export default routes;
