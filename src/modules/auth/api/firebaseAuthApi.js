import axios from "axios";

const apiKey = "AIzaSyCx8eaqFE0KC3XOOUOvrKcCVnP6W4j4W4Y";

// Endpoint LoginContraseñaEmail: /accounts:signInWithPassword

const firebaseApi = axios.create({
  baseURL: `https://identitytoolkit.googleapis.com/v1`,
  params: {
    key: apiKey,
  },
});

export default firebaseApi;
