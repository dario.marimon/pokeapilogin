const routes = {
  name: "auth",
  redirect: { name: "login" },
  component: () => import("../views/AuthLayout.vue"),
  children: [
    {
      path: "login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
    },
    {
      path: "register",
      name: "register",
      component: () => import("../views/RegisterView.vue"),
    },
  ],
};

export default routes;
